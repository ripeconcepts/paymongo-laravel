<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use App\Models\Order;

class PaymentController extends Controller
{
    public function createPaymentIntent(Request $request) {
        $data = [
            'data' => [
                'attributes' => [
                    'amount' => (Integer) $request->amount,
                    'payment_method_allowed' => ['card'],
                    'payment_method_options' => [
                        'card' => [
                            'request_three_d_secure' => 'automatic'
                        ]
                    ],
                    'description' => $request->description,
                    'currency' => $request->currency,
                    // 'statement_descriptor' => '',
                    // 'metadata' => []
                ]
            ]
        ];

        $res = Http::withHeaders([
            'content-type' => 'application/json',
            'authorization' => 'Basic '.base64_encode(env('PM_SECRET_KEY'))
        ])->post('https://api.paymongo.com/v1/payment_intents', $data);

        return $res;
    }

    public function createWebhook() {
        $data = [
            'data' => [
                'attributes' => [
                    'url' => env('APP_URL').'/api/processPayment',
                    'events' => ['source.chargeable']
                ]
            ]
        ];

        $res = Http::withHeaders([
            'content-type' => 'application/json',
            'authorization' => 'Basic '.base64_encode(env('PM_SECRET_KEY'))
        ])->post('https://api.paymongo.com/v1/webhooks', $data);

        return $res;
    }

    public function createPayment(Request $request) {
        $paymongo_signature = $request->header('paymongo-signature');

        if($paymongo_signature) {

            $paymongo_signature = explode(',', $paymongo_signature);
            $timestamp = str_replace('t=', '', $paymongo_signature[0]);
            $test_signature = str_replace('te=', '', $paymongo_signature[1]);
            $live_signature = str_replace('li=', '', $paymongo_signature[2]);

            $my_signature = $timestamp.'.'.$request->getContent();
            $my_signature = hash_hmac('sha256', $my_signature, env('PM_WEBHOOK_SECRET_KEY'));

            if($test_signature === $my_signature) {
                $req = $request->all();

                $srcId = $req['data']['attributes']['data']['id'];
                $description = null;
                if(Order::getOrderBySrcId($srcId)) {
                    $description = Order::getOrderBySrcId($srcId)->description;
                }

                $data = [
                    'data' => [
                        'attributes' => [
                            'amount' => (Integer) $req['data']['attributes']['data']['attributes']['amount'],
                            'description' => $description,
                            'currency' => $req['data']['attributes']['data']['attributes']['currency'],
                            // 'statement_descriptor' => '',
                            'source' => [
                                'id' => $req['data']['attributes']['data']['id'],
                                'type' => 'source'
                            ]
                        ]
                    ]
                ];

                $res = Http::withHeaders([
                    'content-type' => 'application/json',
                    'authorization' => 'Basic '.base64_encode(env('PM_SECRET_KEY'))
                ])->post('https://api.paymongo.com/v1/payments', $data);
                
                if($res) {
                    Order::updatePaymentBySrcId($srcId, $res['data']['id'], $res['data']['attributes']['status']);
                }
                
                return response()->json(['message' => 'ok'], 200);
            } else {
                return response()->json(['message' => 'unauthorized'], 401);
            }
        } else {
            return response()->json(['message' => 'unauthorized'], 401);
        }
    }

}
