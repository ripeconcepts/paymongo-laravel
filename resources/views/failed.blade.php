@extends('layouts.main')
@section('page_title', 'Failed')

@section('content')
    <div class="thankyou-container">
        <h2>Something went wrong!</h2>
        <p><a href="/payment">Please try again or try other payment method</a></p>
    </div>
@endSection