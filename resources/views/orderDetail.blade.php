@extends('layouts.main')
@section('page_title', 'Order Details')

@section('content')
    <order-detail order-id="{{ $id }}" assets-url="{{ asset('images') }}"></order-detail>
@endSection