<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $fillable = [
        'payment_type',
        'name',
        'email',
        'phone',
        'address_line1',
        'address_line2',
        'city',
        'state',
        'postal_code',
        'country',
        'currency',
        'description',
        'amount',
        'piId',
        'pmId',
        'srcId',
        'clientKey',
        'payId',
        'status'
    ];

    public function scopeGetOrderBySrcId($query, $srcId) {
        return $query->where('srcId', $srcId)->first();
    }

    public function scopeUpdatePaymentBySrcId($query, $srcId, $payId, $status) {
        return $query->where('srcId', $srcId)->update([
            'payId' => $payId,
            'status' => 'paid'
        ]);
    }

}
