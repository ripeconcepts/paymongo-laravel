<?php

use Illuminate\Database\Seeder;
use App\User;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'name' => 'Admin',
                'email' => 'admin@ripeconcepts.com',
                'password' => bcrypt('R!pe1234')
            ]
        ];
        foreach($data as $row) {
            User::create($row);
        }
    }
}
