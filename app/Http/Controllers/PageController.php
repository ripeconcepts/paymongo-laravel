<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PageController extends Controller
{
    
    public function catalog() {
        return view('catalog');
    }

    public function cart() {
        return view('cart');
    }

    public function payment() {
        return view('payment');
    }

    public function thankYou() {
        return view('thankYou');
    }

    public function failed() {
        return view('failed');
    }

    public function admin() {
        if(Auth::check()) {
            return redirect('/orders');
        }
        return view('login');
    }

    public function order() {
        return view('orderList');
    }

    public function orderDetail($id) {
        return view('orderDetail', compact('id'));
    }
    
}
