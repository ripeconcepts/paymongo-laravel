<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::post('/request_login', 'AuthController@login');
Route::get('/request_logout', 'AuthController@logout');

Route::get('/', 'PageController@catalog');
Route::get('/cart', 'PageController@cart');
Route::get('/payment', 'PageController@payment');
Route::get('/thank_you', 'PageController@thankYou');
Route::get('/failed', 'PageController@failed');
Route::get('/admin', 'PageController@admin')->name('admin');

Route::get('/products', 'ProductController@index');
Route::post('/recordOrder', 'OrderController@create');
Route::get('/setWebhook', 'PaymentController@createWebhook');
Route::post('/payment_intent', 'PaymentController@createPaymentIntent');
Route::post('/getCart', 'ProductController@getCart');

Route::middleware('auth')->group(function () {
    Route::get('/orders', 'PageController@order');
    Route::get('/orders/{id}', 'PageController@orderDetail');
    Route::get('/getOrders', 'OrderController@index');
    Route::get('/getOrders/{id}', 'OrderController@show');
});