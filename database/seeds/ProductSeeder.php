<?php

use Illuminate\Database\Seeder;
use App\Models\Product;

class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [ 
                'name' => 'iPad Pro',
                'price' => 4040143,
                'image' => 'ipadpro.jpg'
            ],
            [ 
                'name' => 'iPad Air',
                'price' => 2523193,
                'image' => 'ipadair.jpg'
            ],
            [ 
                'name' => 'iPad Mini',
                'price' => 2019558,
                'image' => 'ipadmini.jpg'
            ],
            [ 
                'name' => 'iPhone 11 Pro',
                'price' => 5051443,
                'image' => 'iphone11pro.jpg'
            ],
            [ 
                'name' => 'iPhone 11',
                'price' => 3534493,
                'image' => 'iphone11.jpg'
            ],
            [ 
                'name' => 'iPhone XR',
                'price' => 2272636,
                'image' => 'iphonexr.jpg'
            ]
        ];

        foreach($data as $row) {
            Product::create($row);
        }
    }
}
