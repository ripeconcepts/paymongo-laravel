@extends('layouts.main')
@section('page_title', 'Catalog')

@section('content')
    <catalog assets-url="{{ asset('images') }}"></catalog>
@endSection