<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>@yield('page_title')</title>
    <link rel="stylesheet" href="{{ asset('css/app.css') }}">
</head>
<body>

    <div id="app">
        <navigation logged-in="{{ Auth::check() }}"></navigation>
        <div class="container">
            <notify></notify>
            @yield('content')
        </div>
    </div>

    <script src="{{ asset('js/app.js') }}"></script>
    
</body>
</html>