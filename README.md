## .env

- PM_PUBLIC_KEY=xxxxx
- PM_SECRET_KEY=xxxxx
- PM_WEBHOOK_SECRET_KEY=xxxxx
- MIX_PM_PUBLIC_KEY="${PM_PUBLIC_KEY}"

## Database

- Run command "php artisan migrate --seed"

## Register your Webhook

1. Run "{APP_URL}/setWebhook" once on the browser
2. From the response, copy the WEBHOOK_SECRET_KEY
3. Paste the WEBHOOK_SECRET_KEY to your .env file (PM_WEBHOOK_SECRET_KEY=xxxxx)