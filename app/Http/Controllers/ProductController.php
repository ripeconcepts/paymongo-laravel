<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Product;

class ProductController extends Controller
{

    public function index() {
        return Product::get();
    }

    public function getCart(Request $request) {
        $data = [];
        if($request->cart) {
            foreach($request->cart as $key => $value) {
                $product = Product::find($value['id']);
                if($product) {
                    $data[$key] = [
                        'id' => $value['id'],
                        'qty' => $value['qty'],
                        'name' => $product->name,
                        'price' => $product->price,
                        'new_price' => $value['qty'] * $product->price,
                        'image' => $product->image
                    ];
                }
            }
        }
        return response()->json(['message' => 'ok', 'data' => $data], 200);
    }

}

