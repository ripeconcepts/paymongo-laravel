<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class AuthController extends Controller
{

    public function login(Request $request) {
        $validator = Validator::make($request->all(), [
            'email' => ['required', 'email', 'max:100'],
            'password' => ['required', 'string', 'max:100', 'min:8']
        ]);

        if($validator->fails()) {
            return response()->json(['error' => 'These credentials do not match our records.'], 400);
        } else {
            if(Auth::attempt(['email' => $request->email, 'password' => $request->password])) {
                return response()->json(['message' => 'ok'], 200);
            } else {
                return response()->json(['error' => 'These credentials do not match our records.'], 400);
            }
        }
    }

    public function logout() {
        Auth::logout();
        return redirect('/');
    }

}
