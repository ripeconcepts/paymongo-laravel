<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Order;
use App\Models\OrderDetail;
use Illuminate\Support\Facades\DB;

class OrderController extends Controller
{

    public function index() {
        $orders = Order::orderBy('created_at', 'desc')->get();
        foreach($orders as  $key => $value) {
            if(($value->payment_type == 'gcash' || $value->payment_type == 'grab_pay') && $value->status == 'pending') {
                if($value->created_at->diffInMinutes() >= 80) {
                    $orders[$key]->status = 'failed';
                    $order = Order::find($value->id);
                    $order->status = 'failed';
                    $order->save();
                }
            }
        }
        return $orders;
    }

    public function show($id) {
        $order = Order::find($id);
        if($order) {
            $data = [
                'info' => $order,
                'cart' => OrderDetail::select(
                                'order_details.id', 
                                'products.name', 
                                'order_details.price', 
                                'products.image', 
                                'order_details.quantity',
                                DB::raw('(order_details.price * order_details.quantity) AS new_price')
                            )
                            ->where('order_id', $id)
                            ->join('products', 'order_details.product_id', '=', 'products.id', 'left')
                            ->get()
            ];
            return response()->json(['message' => 'ok', 'data' => $data], 200);
        } else {
            return response()->json(['message' => 'bad request'], 400);
        }
    }

    public function create(Request $request) {
        $order = Order::create([
            'payment_type' => $request->type,
            'name' => $request->name,
            'email' => $request->email,
            'phone' => $request->phone,
            'address_line1' => $request->line1,
            'address_line2' => $request->line2,
            'city' => $request->city,
            'state' => $request->state,
            'postal_code' => $request->postal_code,
            'country' => $request->country,
            'currency' => $request->currency,
            'description' => $request->description,
            'amount' => $request->amount,
            'piId' => $request->piId,
            'pmId' => $request->pmId,
            'srcId' => $request->srcId,
            'clientKey' => $request->clientKey,
            'payId' => $request->payId,
            'status' => $request->status
        ]);

        if($order) {
            if($request->orders) {
                foreach($request->orders as $row) {
                    OrderDetail::create([
                        'order_id' => $order->id,
                        'product_id' => $row['id'],
                        'quantity' => $row['qty'],
                        'price' => $row['price']
                    ]);
                }
            }
            return response()->json(['message' => 'ok'], 200);
        } else {
            return response()->json(['message' => 'bad request'], 400);
        }
    }

}
