/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');
window.headers = {
    'content-type': 'application/json',
    'authorization': `Basic ${btoa(process.env.MIX_PM_PUBLIC_KEY)}`
}

Vue.use(require('vue-moment'));

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))

Vue.component('navigation', require('./components/Navigation').default);
Vue.component('notify', require('./components/Notify').default);
Vue.component('catalog', require('./components/Catalog').default);
Vue.component('cart', require('./components/Cart').default);
Vue.component('payment', require('./components/Payment').default);
Vue.component('thank-you', require('./components/ThankYou').default);
Vue.component('login', require('./components/Login').default);
Vue.component('order-list', require('./components/OrderList').default);
Vue.component('order-detail', require('./components/OrderDetail').default);

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

const app = new Vue({
    el: '#app',
});
